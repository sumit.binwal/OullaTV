//
//  UIStoryBoardExtention.swift
//  MyanCareDoctor
//
//  Created by Sumit Sharma on 15/12/2017.
//  Copyright © 2017 sumit. All rights reserved.
//

import Foundation
import UIKit

/// MARK: - UIStoryboard extension
extension UIStoryboard
{
    /// function to get Login StoryBoard
    ///
    /// - Returns: UIStoryboard value
    static func getLoginStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "Login", bundle: nil)
        return storyBoard
    }
    
    
    static func getMainStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "Main", bundle: nil)
        return storyBoard
    }

    
    /// function to get Login StoryBoard
    ///
    /// - Returns: UIStoryboard value
    static func getSideMenuStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "SideMenu", bundle: nil)
        return storyBoard
    }
    
    
    
    /// function to get Signup StoryBoard
    ///
    /// - Returns: UIStoryboard value
    static func getSignUpStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "SignUp", bundle: nil)
        return storyBoard
    }
}
