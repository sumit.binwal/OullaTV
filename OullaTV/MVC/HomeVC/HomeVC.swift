//
//  HomeVC.swift
//  OuulaTv
//
//  Created by Sevta on 20/07/19.
//  Copyright © 2019 Sevta. All rights reserved.
//

import UIKit

class HomeVC: UIViewController {

    @IBOutlet var collectionViewHeader: UICollectionView!
    
    @IBOutlet var tableViewHome: UITableView!
    @IBOutlet var pageControl: UIPageControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.title = "Ouula TV"
        
        
        
        tableViewHome.register(UINib(nibName: "HomeTableViewCell", bundle: nil), forCellReuseIdentifier: "HomeTableViewCell")
        tableViewHome.register(UINib(nibName: "CategoryTableViewCell", bundle: nil), forCellReuseIdentifier: "CategoryTableViewCell")
    }

    @IBAction func searchBtnClicked(_ sender: Any) {
        
        
        
        let searchVC = UIStoryboard.getMainStoryBoard().instantiateViewController(withIdentifier: "SearchFilterVC") as! SearchFilterVC
        self.navigationController?.pushViewController(searchVC, animated: true)
    }
    @IBAction func menuBtnClicked(_ sender: Any) {
                let sideMenu = UIStoryboard.getSideMenuStoryBoard().instantiateInitialViewController() as! SideMenuViewController
                let navController = UINavigationController.init(rootViewController: sideMenu)
                present(navController, animated: true, completion: nil)

    }
    @IBAction func homeButtonClicked(_ sender: Any) {
        
        let storyboard = UIStoryboard.init(name: "Teams", bundle: nil)
        let teamsVC = storyboard.instantiateInitialViewController() as! TeamsVC
        self.navigationController?.pushViewController(teamsVC, animated: true)
    }
    
    @IBAction func moviesButtonClicked(_ sender: Any) {
        
        let storyboard = UIStoryboard.init(name: "Category", bundle: nil)
        let categoryVC = storyboard.instantiateViewController(withIdentifier: "CategoryVC") as! CategoryVC
        self.navigationController?.pushViewController(categoryVC, animated: true)
    }
    
    @IBAction func seriesButtonClicked(_ sender: Any) {
        
    }
    
    @IBAction func sportsButtonClicked(_ sender: Any) {
    }
    
    @IBAction func documentaryButtonClicked(_ sender: Any) {
    }
}

extension HomeVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collectionViewHeader {
            return CGSize(width: UIScreen.main.bounds.size.width, height: 203)
        } else {
            return CGSize(width: 149, height: 164)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collectionViewHeader {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HeaderCollectionViewCell", for: indexPath) as! HeaderCollectionViewCell
            
            return cell
        } else {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath) as! HomeCollectionViewCell
            
            cell.layer.cornerRadius = 3.0
            cell.clipsToBounds = true
            
            cell.labelLive.layer.cornerRadius = 3.0
            cell.labelLive.clipsToBounds = true
            
            if collectionView.tag == 100 {
                cell.labelLive.isHidden = false
            } else {
                cell.labelLive.isHidden = true
            }
            
            return cell
        }
        
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard = UIStoryboard.init(name: "VideoSinglePage", bundle: nil)
        let videoSinglePageVC = storyboard.instantiateInitialViewController() as! VideoSinglePageVC
        self.navigationController?.pushViewController(videoSinglePageVC, animated: true)

    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if collectionView == collectionViewHeader {
            self.pageControl.currentPage = indexPath.row
        }
    }
}


extension HomeVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 237.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell", for: indexPath) as! HomeTableViewCell
        
        cell.labelTitle.textColor = .white
        
        cell.collectionViewCell.delegate = self
        cell.collectionViewCell.dataSource = self
        
        cell.collectionViewCell.register(UINib(nibName: "HomeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HomeCollectionViewCell")
        cell.collectionViewCell.tag = indexPath.row + 100
        
        switch indexPath.row {
        case 0:
            cell.labelTitle.text = "Live Now"
        case 1:
            cell.labelTitle.text = "Continue Watching"
        case 2:
            cell.labelTitle.text = "Category Teasers"
        default:
            break
        }
        
        cell.selectionStyle = .none
        return cell
    }
    
}
