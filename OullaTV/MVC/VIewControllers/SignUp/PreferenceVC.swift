//
//  PreferenceVC.swift
//  OullaTV
//
//  Created by MAC on 20/07/19.
//  Copyright © 2019 Sumit Sharma. All rights reserved.
//

import UIKit
import iCarousel

class PreferenceVC: UIViewController {

    @IBOutlet weak var signUpTableView: UITableView!
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var footerView: UIView!
    
    var items: [Int] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func setUpView() {
        headerView.frame = CGRect.init(x: headerView.frame.origin.x, y: headerView.frame.origin.y, width: UIScreen.main.bounds.size.width, height: headerView.frame.size.height * scaleFactorX)
        
        footerView.frame = CGRect.init(x: footerView.frame.origin.x, y: footerView.frame.origin.y, width: UIScreen.main.bounds.size.width, height: footerView.frame.size.height * scaleFactorX)
        
        signUpTableView.delegate = self
        signUpTableView.dataSource = self
        
        for i in 0 ... 9 {
            items.append(i)
        }
    }
    
    @IBAction func okButtonClick(_ sender: Any) {
        let dashboard = UIStoryboard.getMainStoryBoard().instantiateInitialViewController()
        APPDELEGATE.window?.rootViewController = dashboard

    }
    
    @IBAction func closeButtonClick(_ sender: Any) {
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
}

extension PreferenceVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PreferencesTableViewCell") as! PreferencesTableViewCell
        
        cell.iCarouselView.delegate = self
        cell.iCarouselView.dataSource = self
        
        cell.iCarouselView.type = .rotary
        
        return cell
    }
}

extension PreferenceVC: iCarouselDataSource, iCarouselDelegate {
    
    func numberOfItems(in carousel: iCarousel) -> Int {
        return items.count
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        
        var label: UILabel
        var itemView: UIImageView
        
        //reuse view if available, otherwise create a new view
        if let view = view as? UIImageView {
            itemView = view
            view.image = UIImage.init(named: "sliderImg")
            //get a reference to the label in the recycled view
            label = itemView.viewWithTag(1) as! UILabel
        }
        else {
            //don't do anything specific to the index within
            //this `if ... else` statement because the view will be
            //recycled and used with other index values later
            itemView = UIImageView(frame: CGRect(x: 0, y: 0, width: 250 * scaleFactorX, height: 274 * scaleFactorX))
            itemView.image = UIImage(named: "sliderImg")
            //itemView.backgroundColor = UIColor.red
            itemView.contentMode = .center
        }
        
        return itemView
    }
    
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        if (option == .spacing) {
            return value * 1.1
        }
        return value
    }
}
