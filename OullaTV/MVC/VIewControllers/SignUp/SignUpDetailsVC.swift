//
//  SignUpDetailsVC.swift
//  OullaTV
//
//  Created by MAC on 20/07/19.
//  Copyright © 2019 Sumit Sharma. All rights reserved.
//

import UIKit

class SignUpDetailsVC: UIViewController {

    @IBOutlet weak var signUpTableView: UITableView!
    @IBOutlet weak var proceedButton: UIButton!
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var footerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func setUpView() {
        headerView.frame = CGRect.init(x: headerView.frame.origin.x, y: headerView.frame.origin.y, width: UIScreen.main.bounds.size.width, height: headerView.frame.size.height * scaleFactorX)
        
        footerView.frame = CGRect.init(x: footerView.frame.origin.x, y: footerView.frame.origin.y, width: UIScreen.main.bounds.size.width, height: footerView.frame.size.height * scaleFactorX)
        
        proceedButton.layer.borderColor = UIColor.init(red: 142.0/255.0, green: 89.0/255.0, blue: 177.0/255.0, alpha: 1.0).cgColor
        proceedButton.layer.borderWidth = 1.0
        proceedButton.layer.masksToBounds = true
        proceedButton.layer.cornerRadius = (proceedButton.frame.size.height / 2) * scaleFactorX
        
        signUpTableView.delegate = self
        signUpTableView.dataSource = self
        
        signUpTableView.register(UINib.init(nibName: "LoginTableViewCell", bundle: nil), forCellReuseIdentifier: "LoginTableViewCell")
        signUpTableView.register(UINib.init(nibName: "MobileNoTableViewCell", bundle: nil), forCellReuseIdentifier: "MobileNoTableViewCell")
        signUpTableView.register(UINib.init(nibName: "LocationTableViewCell", bundle: nil), forCellReuseIdentifier: "LocationTableViewCell")
    }
    
    @IBAction func proceedButtonClick(_ sender: Any) {
        let contentPreferenceVC = UIStoryboard.getSignUpStoryBoard().instantiateViewController(withIdentifier: "SignUpContentPreferenceVC") as! SignUpContentPreferenceVC
        self.navigationController?.pushViewController(contentPreferenceVC, animated: true)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
}

extension SignUpDetailsVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "LoginTableViewCell") as! LoginTableViewCell
        
        var cellType = cell.cellType
        
        switch indexPath.row {
        
        case 0:
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "LocationTableViewCell") as! LocationTableViewCell
            return cell1
            
        case 1:
            cellType = .name
            
        case 2:
            cellType = .email
            
        case 3:
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "MobileNoTableViewCell") as! MobileNoTableViewCell
            return cell1
            
        case 4:
            cellType = .houseNo
            
        case 5:
            cellType = .address
            
        case 6:
            cellType = .city
            
        case 7:
            cellType = .zipCode
            
        default:
            cellType = .none
        }
        
        cell.cellType = cellType
        
        return cell
    }
}

