//
//  MySubscriptionViewController.swift
//  OullaTV
//
//  Created by Sumit Sharma on 21/07/19.
//  Copyright © 2019 Sumit Sharma. All rights reserved.
//

import UIKit

class MySubscriptionViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
self.navigationController?.navigationBar.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backBtnClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension MySubscriptionViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SubscriptionCell") as! SubscriptionCell
        cell.selectionStyle = .none
        switch indexPath.row {
        case 0:
            cell.imgVwTop.isHidden = true
            cell.imgVwBottom.isHidden = true
            cell.labelHeading.text = "Basic Subscription"
            cell.labelDetail1.text = "•  1 active device"
            cell.labelDetail2.text = "•  Unlimited Streaming"
            cell.labelDetail3.isHidden = true
            cell.labelPrice.text = "$10 per month"
            
        case 1:
            cell.imgVwTop.isHidden = false
            cell.imgVwBottom.isHidden = false

            cell.labelHeading.text = "Standard Subscription"
            cell.labelDetail1.text = "•  5 active device"
            cell.labelDetail2.text = "•  Unlimited Streaming"
            cell.labelDetail3.text = "•  Downloadable Content"
            cell.labelPrice.text = "$20 per month"
            
        case 2:
            cell.imgVwTop.isHidden = true
            cell.imgVwBottom.isHidden = true

            cell.labelHeading.text = "Standard Subscription"
            cell.labelDetail1.text = "•  5 active device"
            cell.labelDetail2.text = "•  Unlimited Streaming"
            cell.labelDetail3.text = "•  Downloadable Content"
            cell.labelPrice.text = "$10 per month"
        default:
            break
        }
        
        return cell
    }
}

