//
//  PaymentViewController.swift
//  OullaTV
//
//  Created by Sumit Sharma on 21/07/19.
//  Copyright © 2019 Sumit Sharma. All rights reserved.
//

import UIKit

class PaymentViewController: UIViewController {

    
    @IBOutlet weak var signUpTableView: UITableView!
    @IBOutlet weak var proceedButton: UIButton!
    
    @IBOutlet weak var footerView: UIView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true

        setUpView()

        // Do any additional setup after loading the view.
    }
    @IBAction func backBtnClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }


    func setUpView() {
        
        footerView.frame = CGRect.init(x: footerView.frame.origin.x, y: footerView.frame.origin.y, width: UIScreen.main.bounds.size.width, height: footerView.frame.size.height * scaleFactorX)
        
        proceedButton.layer.borderColor = UIColor.init(red: 142.0/255.0, green: 89.0/255.0, blue: 177.0/255.0, alpha: 1.0).cgColor
        proceedButton.layer.borderWidth = 1.0
        proceedButton.layer.masksToBounds = true
        proceedButton.layer.cornerRadius = (proceedButton.frame.size.height / 2) * scaleFactorX
        
        signUpTableView.delegate = self
        signUpTableView.dataSource = self
        
        signUpTableView.register(UINib.init(nibName: "LoginTableViewCell", bundle: nil), forCellReuseIdentifier: "LoginTableViewCell")
        signUpTableView.register(UINib.init(nibName: "CardExpiryTableViewCell", bundle: nil), forCellReuseIdentifier: "CardExpiryTableViewCell")
        signUpTableView.register(UINib.init(nibName: "CardTypeTableViewCell", bundle: nil), forCellReuseIdentifier: "CardTypeTableViewCell")
    }

}

extension PaymentViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "LoginTableViewCell") as! LoginTableViewCell
        
        var cellType = cell.cellType
        
        switch indexPath.row {
            
        case 0:
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "CardTypeTableViewCell") as! CardTypeTableViewCell
            return cell1
            
        case 1:
            cellType = .cardNumber
            
        case 2:
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "CardExpiryTableViewCell") as! CardExpiryTableViewCell
            return cell1
            
        case 3:
            cellType = .cvcNumber
            
        default:
            cellType = .none
        }
        
        cell.cellType = cellType
        
        return cell
    }
}
