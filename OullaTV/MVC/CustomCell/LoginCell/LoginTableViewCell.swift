//
//  LoginTableViewCell.swift
//  OullaTV
//
//  Created by MAC on 20/07/19.
//  Copyright © 2019 Sumit Sharma. All rights reserved.
//

import UIKit

class LoginTableViewCell: UITableViewCell {
    
    enum LoginCellType {
        case email
        case password
        case name
        case houseNo
        case address
        case city
        case zipCode
        case cardNumber
        case cvcNumber
        case none
    }
    
    var cellType: LoginCellType = .none

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var textBgView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.selectionStyle = .none
        
        textBgView.backgroundColor = UIColor.init(red: 22.0/255.0, green: 52.0/255.0, blue: 62.0/255.0, alpha: 1.0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        switch cellType {
            
        case .email:
            nameLabel.text = "Email Address"
            textField.attributedPlaceholder = NSAttributedString(string: "example@gmail.com", attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(white: 255, alpha: 0.58)])
            textField.isSecureTextEntry = false
            
        case .password:
            nameLabel.text = "Password"
            textField.attributedPlaceholder = NSAttributedString(string: "........", attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(white: 255, alpha: 0.58)])
            textField.isSecureTextEntry = true
            
        case .name:
            nameLabel.text = "Your Name"
            textField.attributedPlaceholder = NSAttributedString(string: "Example Text", attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(white: 255, alpha: 0.58)])
            textField.isSecureTextEntry = false
            
        case .houseNo:
            nameLabel.text = "House/flat number"
            textField.attributedPlaceholder = NSAttributedString(string: "Example Text", attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(white: 255, alpha: 0.58)])
            textField.isSecureTextEntry = false
            
        case .address:
            nameLabel.text = "Address"
            textField.attributedPlaceholder = NSAttributedString(string: "Example Text", attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(white: 255, alpha: 0.58)])
            textField.isSecureTextEntry = false
            
        case .city:
            nameLabel.text = "City"
            textField.attributedPlaceholder = NSAttributedString(string: "Example Text", attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(white: 255, alpha: 0.58)])
            textField.isSecureTextEntry = false
            
        case .zipCode:
            nameLabel.text = "Zipcode"
            textField.attributedPlaceholder = NSAttributedString(string: "Example Text", attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(white: 255, alpha: 0.58)])
            textField.isSecureTextEntry = false
            
        case .cardNumber:
            nameLabel.text = "16 Digits Card Number"
            textField.attributedPlaceholder = NSAttributedString(string: ".... .... .... ....", attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(white: 255, alpha: 0.58)])
            textField.isSecureTextEntry = false
            
        case .cvcNumber:
            nameLabel.text = "CVC Number"
            textField.attributedPlaceholder = NSAttributedString(string: "Example Text", attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(white: 255, alpha: 0.58)])
            textField.isSecureTextEntry = false
            
        default:
            break
        }
    }
}
