//
//  ContentPreferenceTableViewCell.swift
//  OullaTV
//
//  Created by MAC on 20/07/19.
//  Copyright © 2019 Sumit Sharma. All rights reserved.
//

import UIKit

class ContentPreferenceTableViewCell: UITableViewCell {
    
    @IBOutlet weak var switchOnOff: UISwitch!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.selectionStyle = .none
        
       // switchOnOff.onTintColor = UIColor.init(red: 142.0/255.0, green: 89.0/255.0, blue: 177.0/255.0, alpha: 1.0)
        
//        switchOnOff.layer.cornerRadius = switchOnOff.frame.size.height / 2
//        switchOnOff.layer.borderWidth = 1.0
//        switchOnOff.layer.borderColor = UIColor.clear.cgColor
//        switchOnOff.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    @IBAction func switchAction(_ sender: Any) {
        
    }
}
