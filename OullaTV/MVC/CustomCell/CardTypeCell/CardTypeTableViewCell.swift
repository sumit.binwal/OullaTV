//
//  CardTypeTableViewCell.swift
//  OullaTV
//
//  Created by MAC on 20/07/19.
//  Copyright © 2019 Sumit Sharma. All rights reserved.
//

import UIKit

class CardTypeTableViewCell: UITableViewCell {

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var cardTypeImageView: UIImageView!
    @IBOutlet weak var textBgView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.selectionStyle = .none
        
        nameLabel.text = "Card Type"
        textField.attributedPlaceholder = NSAttributedString(string: "", attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(white: 255, alpha: 0.58)])
        textField.isSecureTextEntry = false
        
        textBgView.backgroundColor = UIColor.init(red: 22.0/255.0, green: 52.0/255.0, blue: 62.0/255.0, alpha: 1.0)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
