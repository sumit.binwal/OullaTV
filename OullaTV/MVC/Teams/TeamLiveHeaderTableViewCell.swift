//
//  TeamLiveHeaderTableViewCell.swift
//  OuulaTv
//
//  Created by MAC on 21/07/19.
//  Copyright © 2019 Sevta. All rights reserved.
//

import UIKit

class TeamLiveHeaderTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
