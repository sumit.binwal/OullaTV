//
//  TeamsVC.swift
//  OuulaTv
//
//  Created by MAC on 21/07/19.
//  Copyright © 2019 Sevta. All rights reserved.
//

import UIKit

class TeamsVC: UIViewController {
    
    @IBOutlet var teamTableView: UITableView!
    
    @IBOutlet weak var saveTeamButton: UIButton!
    @IBOutlet var headerView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    
    func setUpView() {
        
        headerView.frame = CGRect.init(x: headerView.frame.origin.x, y: headerView.frame.origin.y, width: UIScreen.main.bounds.size.width, height: headerView.frame.size.height * scaleFactorX)
        
        teamTableView.delegate = self
        teamTableView.dataSource = self
        
        saveTeamButton.layer.borderColor = UIColor.clear.cgColor
        saveTeamButton.layer.borderWidth = 1.0
        saveTeamButton.layer.masksToBounds = true
        saveTeamButton.layer.cornerRadius = (saveTeamButton.frame.size.height / 2) * scaleFactorX
    }
    
    @IBAction func saveTeamButtonAction(_ sender: Any) {
    }
}

extension TeamsVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            let headrCell = tableView.dequeueReusableCell(withIdentifier: "TeamLiveHeaderTableViewCell") as! TeamLiveHeaderTableViewCell
            return headrCell
        }
        else {
            let headrCell = tableView.dequeueReusableCell(withIdentifier: "TeamPreviousHeaderTableViewCell") as! TeamPreviousHeaderTableViewCell
            return headrCell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 50 * scaleFactorX
        }
        else {
            return 75 * scaleFactorX
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        else {
            return 5
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TeamsTableViewCell") as! TeamsTableViewCell
        
        if indexPath.section == 0 {
            cell.liveBgView.isHidden = false
        }
        else {
            cell.liveBgView.isHidden = true
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100 * scaleFactorX
    }
}
